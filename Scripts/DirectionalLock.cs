using UnityEngine;
using System.Linq;

public class DirectionalLock : MonoBehaviour
{
    [SerializeField]
    private int numberOfDirections;

    // TODO: create Direction type
    [SerializeField]
    private int[] solution;
    private int[] currentCombination;
    private int lastInputIndex = 0;
    private bool solved = false;

    void Start()
    {
        currentCombination = new int[numberOfDirections];
    }

    public void Reset()
    {
        currentCombination = new int[numberOfDirections];
        lastInputIndex = 0;
    }

    public bool trySolve()
    {
        if (Enumerable.SequenceEqual(solution, currentCombination))
        {
            solved = true;
        }
        Reset();
        return solved;
    }

    public bool isSolved()
    {
        return solved;
    }

    public void InputDirection(int direction)
    {
        if (lastInputIndex >= currentCombination.Length) {
            // Send event?
        } else {
            currentCombination[lastInputIndex] = direction;
            lastInputIndex++;
        }
    }

    public void Unlock()
    {
        Debug.Log("Unlocking door using Directional Lock");
        // Disable player input map
        // Enable directional lock input map
        // Raise event to trigger gfx (vignette, darker background, other post processing)
        // Show UI
    }

    void Solve()
    {
        if (trySolve())
        {
            // Enable player input map
            // Disable directional lock input map
            // Raise event to reset gfx (vignette, darker background, other post processing)
            // Hide UI
            // Raise event to unlock door
        }
    }

}
