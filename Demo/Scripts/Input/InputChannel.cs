using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "InputChannel", menuName = "Channels/InputChannel", order = 0)]
public class InputChannel : ScriptableObject, InputActions.IPlayerActions, InputActions.IDirectionalLockActions
{
    // Assign empty delegate to avoid null checks upon usage

    // Player Events to Invoke
    public UnityAction<bool> InteractEvent = delegate { };
    public UnityAction<Vector2> MoveEvent = delegate { };

    // Player Events to Invoke
    public UnityAction<Vector2> DirectionEvent = delegate { };
    public UnityAction<bool> SolveEvent = delegate { };

    private InputActions gameInput;

    private void OnEnable()
    {
        if (gameInput == null) {
            gameInput = new InputActions();
            gameInput.Player.SetCallbacks(this);
            gameInput.DirectionalLock.SetCallbacks(this);
        }
        gameInput.Enable();
    }

    private void OnDisable()
    {
        gameInput.Player.Disable();
        gameInput.DirectionalLock.Disable();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        Debug.Log("Invoking Move Event " + context.ReadValue<Vector2>());
        MoveEvent.Invoke(context.ReadValue<Vector2>());
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        Debug.Log("Invoking Interact Event");
        InteractEvent.Invoke(context.performed);
    }

    public void OnDirection(InputAction.CallbackContext context)
    {
        Debug.Log("Invoking Direction Event");
    }

    public void OnSolve(InputAction.CallbackContext context)
    {
        Debug.Log("Invoking Solve Event");
    }
}
