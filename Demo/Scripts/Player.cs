using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private InputChannel inputChannel;
    [SerializeField]
    private float speed = 1.0f;
    private Vector2 currentMoveDirection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentMoveDirection == Vector2.left)
        {
            Debug.Log("Input received to move left");
        }
        Vector3 direction = currentMoveDirection;
        Vector3 currentPosition = this.transform.position;
        Vector3 targetPosition = currentPosition + direction;
        this.transform.position = Vector3.MoveTowards(currentPosition, targetPosition, speed * Time.deltaTime);
    }

    //Adds listeners for events being triggered in the InputChannel script
	private void OnEnable()
	{
		inputChannel.MoveEvent += OnMove;
	}

    private void OnMove(Vector2 moveDirection)
    {
        currentMoveDirection = moveDirection;
    }

}
