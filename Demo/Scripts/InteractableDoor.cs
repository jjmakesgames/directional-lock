using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDoor : MonoBehaviour
{
    [SerializeField]
    private InputChannel inputChannel;

    private InteractionZone interactionZone;
    //private InteractionUI interactionUI;
    private DirectionalLock directionalLock;

    private bool interacted = false;
    private bool unlocking = false;

    // Start is called before the first frame update
    void Start()
    {
        interactionZone = this.GetComponentInChildren<InteractionZone>();
        directionalLock = this.GetComponent<DirectionalLock>();
    }

    // Update is called once per frame
    void Update()
    {
        if (interactionZone.hasPlayerEnteredInteractionZone()) 
        {
            if (interacted)
            {
                unlocking = true;
                directionalLock.Unlock();
            }
        }
    }

    void UnlockDoor()
    {
        if (unlocking)
        {
            Destroy(this.gameObject);
        }
    }

    //Adds listeners for events being triggered in the InputChannel script
	private void OnEnable()
	{
		inputChannel.InteractEvent += OnInteract;
	}

    private void OnInteract(bool interactButtonPressed)
    {
        /**
        * If we are within the interaction zone and this object was not yet
        * interacted with, we set "interacted" to true when the Interact input
        * is received.
        */
        if (interactionZone.hasPlayerEnteredInteractionZone() && !interacted)
        {
            interacted = interactButtonPressed;
        }
    }
}
