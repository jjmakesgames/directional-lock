using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionZone : MonoBehaviour
{
    private bool playerEnteredInteractionZone = false;
    private 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            playerEnteredInteractionZone = true;
            Debug.Log("Player entered interaction zone: " + playerEnteredInteractionZone);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            playerEnteredInteractionZone = false;
            Debug.Log("Player entered interaction zone: " + playerEnteredInteractionZone);
        }
    }

    public bool hasPlayerEnteredInteractionZone()
    {
        return playerEnteredInteractionZone;
    }
}
